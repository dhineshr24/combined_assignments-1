package assignment_4.Second;

import protoPackage.BuildingProto;
import protoPackage.EmployeeProto;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.HashMap;


/*
Prerequisites:
1> A Hbase table "EmployeeProto" should exist with one column family "employeeFamily" and one column with
name "ProtoObject"
2> A Hbase table "BuildingProto" should exist with one column family "buildingFamily" and one column with
name "ProtoObject"
or change the static variables set
-> Can use CreateTable class in Part1 package to create hbase table
 */

//Added specific exception handling in catch instead of generic exceptions
public class JoinDriver extends Configured implements Tool {
    private final static String employeeTableName = "EmployeeProto";
    private final static String buildingTableName = "BuildingProto";
    private final static String employeeColumnFamily = "employeeFamily";
    private final static String buildingColumnFamily = "buildingFamily";
    private final static String columnName = "ProtoObject";
    public static class JoinJobMapper extends TableMapper<ImmutableBytesWritable, Result> {
        public void map(ImmutableBytesWritable key, Result value, Context context) throws IOException, InterruptedException {
            context.write(key,value);
        }
    }

    public static class JoinJobReducer extends TableReducer<ImmutableBytesWritable, Result, ImmutableBytesWritable> {
        HashMap<String, String> buildingInfo = new HashMap<>();

        @Override
        protected void setup(Context context) throws IOException {
            Connection hbaseCon = ConnectionFactory.createConnection(new HBaseConfiguration().create());
            Table table = hbaseCon.getTable(TableName.valueOf(Bytes.toBytes(buildingTableName)));
            for (int buildingNumber = 1; buildingNumber <= 10; buildingNumber++) {
                Result result = table.get(new Get(Bytes.toBytes("B" + buildingNumber)));
                byte[] value = result.getValue(Bytes.toBytes(buildingColumnFamily), Bytes.toBytes(columnName));
                BuildingProto.BuildingDatabase dbo = BuildingProto.BuildingDatabase.parseFrom(value);
                //System.out.println(dbo.getBuilding(0).getCafteriaCode());
                buildingInfo.put(dbo.getBuilding(0).getBuildingCode(), dbo.getBuilding(0).getCafteriaCode());

            }
        }
        @Override
        public void reduce(ImmutableBytesWritable key, Iterable<Result> values, Context context)
                throws IOException, InterruptedException {
            for(Result result: values){
                byte[] value = result.getValue(Bytes.toBytes(employeeColumnFamily), Bytes.toBytes(columnName));
                EmployeeProto.EmployeeDatabase dbo = EmployeeProto.EmployeeDatabase.parseFrom(value);
                String empId = dbo.getEmployee(0).getEmployeeId();
                String name = dbo.getEmployee(0).getName();
                String buildingCode = dbo.getEmployee(0).getBuildingCode();
                EmployeeProto.EmployeeDetails.Floor floorVal = dbo.getEmployee(0).getFloorNo();
                int salary = dbo.getEmployee(0).getSalary();
                String department = dbo.getEmployee(0).getDepartment();
                String cafteriaCode = buildingInfo.get(buildingCode);
                EmployeeProto.EmployeeDetails newEmpObj = EmployeeProto.EmployeeDetails.newBuilder()
                        .setEmployeeId(empId)
                        .setName(name).setBuildingCode(buildingCode)
                        .setFloorNo(floorVal)
                        .setSalary(salary)
                        .setDepartment(department)
                        .setCafeteria(cafteriaCode)
                        .build();
                EmployeeProto.EmployeeDatabase newDbo = EmployeeProto.EmployeeDatabase.newBuilder()
                        .addEmployee(newEmpObj).build();
                Put put = new Put(Bytes.toBytes(empId));
                put.addColumn(Bytes.toBytes(employeeColumnFamily), Bytes.toBytes(columnName),
                        newDbo.toByteArray());
                context.write(new ImmutableBytesWritable(Bytes.toBytes(employeeTableName)),put);
            }
        }


    }

    public int run(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        Scan scan = new Scan();
        scan.setCaching(500);
        scan.setCacheBlocks(false);
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        job.setJarByClass(JoinDriver.class);
        TableMapReduceUtil.initTableMapperJob(TableName.valueOf(employeeTableName),scan,JoinJobMapper.class,
                ImmutableBytesWritable.class,Result.class,job);

        TableMapReduceUtil.initTableReducerJob(employeeTableName,
                JoinJobReducer.class,
                job);
        job.waitForCompletion(true);
        return 0;
    }

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        try {
            JoinDriver runJob = new JoinDriver();
            runJob.run(args);
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
        catch(InterruptedException ex){
            ex.printStackTrace();
        }
        catch(ClassNotFoundException ex){
            ex.printStackTrace();
        }
    }

}