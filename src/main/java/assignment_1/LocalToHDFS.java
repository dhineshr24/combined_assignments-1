package assignment_1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;

class LocalToHDFS {
    public void copyFromLocal() throws IOException{
        String localPath="/Users/taurus/Desktop/Assignment/src/main/java/assignment_1/CsvFiles";
        String uri ="hdfs://localhost:9000";
        String hdfsDir="hdfs://localhost:9000/";
        Configuration conf=new Configuration();
        FileSystem fs=FileSystem.get(URI.create(uri),conf);
        fs.copyFromLocalFile(new Path(localPath),new Path(hdfsDir));
    }
}