package assignment_1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

public class HBaseUtils {

    int fileStartIndex;
    int fileFinalIndex;
    byte[] tableNameByteArray;
    byte[] columnFamilyByteArray;
    String splitBy;

    private List<String> columnNames;
    Configuration config;
    Connection conn;
    TableName tableName;
    Admin admin;

    static int rowID = 1;

    public HBaseUtils() {
        this.splitBy = ",";
        this.fileStartIndex = 1;
        this.fileFinalIndex = 100;
        this.tableName = TableName.valueOf(Bytes.toBytes("EmployeeDetails"));
        this.columnFamilyByteArray = Bytes.toBytes("PersonalDetails");
        this.config = HBaseConfiguration.create();

    }

    public void insertCSVDataToHbase(String hdfsDestination, Configuration conf) throws IOException {
        FileSystem hdfs = null;
        BufferedReader csvReader = null;
        Table table = null;
        FSDataInputStream in = null;

        try {
            hdfs = FileSystem.get(conf);
            conn = ConnectionFactory.createConnection(config);
            tableName = TableName.valueOf(tableNameByteArray);
            admin = conn.getAdmin();
            table = conn.getTable(tableName);
            System.out.println(table);

            initializeHbaseTable(admin);
            for (int i = fileStartIndex; i < fileFinalIndex; i++) {
                Path filePath = new Path(hdfsDestination + "/PFile_" + i + ".csv");
                in = hdfs.open(filePath);
                csvReader = new BufferedReader(new InputStreamReader(in));
                String record;
                setColumnNames(csvReader.readLine());

                while ((record = csvReader.readLine()) != null) {
                    String[] empDetails = record.split(splitBy);
                    insertRow(empDetails, table, rowID);
                    rowID++;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            assert in != null;
            assert csvReader != null;

            in.close();
            csvReader.close();
            hdfs.close();
            conn.close();

        }

    }

    private void insertRow(String[] empDetails, Table table, int rowID) throws IOException {

        for (int i = 0; i < columnNames.size(); i++) {
            table.put(new Put(Bytes.toBytes("rowID_" + rowID)).addColumn(columnFamilyByteArray,
                    Bytes.toBytes(columnNames.get(i)), Bytes.toBytes(empDetails[i])));
            System.out.println("Inserted row with id: " + rowID);
        }

    }

    private void setColumnNames(String header) {
        if (header != null) {
            String[] cols = header.split(splitBy);
            columnNames = Arrays.asList(cols);
        }
    }

    private void initializeHbaseTable(Admin admin) throws IOException {
        if(!admin.tableExists(this.tableName)) {
            TableDescriptor desc = TableDescriptorBuilder.newBuilder(this.tableName)
                    .setColumnFamily(ColumnFamilyDescriptorBuilder.of(columnFamilyByteArray))
                    .build();
            admin.createTable(desc);
            System.out.println("LOG_INFO: Table has been created!");
        }
    }

}
